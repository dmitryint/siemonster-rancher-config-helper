FROM python:2-alpine

COPY ./scripts /scripts
RUN pip install -r /scripts/requirements.txt
WORKDIR /scripts
CMD [ "python", "srv_init.py" ]
